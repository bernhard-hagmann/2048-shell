/**
 * @file   client.c
 * @author Bernhard Hagmann (1325774) <e1325774@student.tuwien.ac.at>
 * @brief  Client for 2048
 * @date   2014-12-24
 */

#include <math.h>
#include <time.h>
#include "common.h"

// ============================================================================
// Defines
// ============================================================================

// ============================================================================
// Structs
// ============================================================================

/** Contains the parsed command line options */
struct options
{
    /** Defines if a new game should be started */
    bool new_game;
    /** Defines the game ID that we should connect to */
    int game_id;
};

// ============================================================================
// Global variables
// ============================================================================

/** The game id */
static int id;

// ============================================================================
// Prototypes
// ============================================================================

// program management
int main(int argc, char *argv[]);
static void parse_options(int argc, char **argv, struct options *opts);
static void allocate_resources(void);
static void usage(void);
static sem_t *sem_open_or_die(char *name);
static void sem_timedwait_or_die(sem_t *sem);
static char get_input_won(void);
static void await_response(enum command cmd);

// printing and user
static void play(enum game_state state);
static void print_game(int FIELD);
static char get_input(void);
static bool is_command(char c);

// ============================================================================
// Implementations
// ============================================================================

/**
 * @brief The programs entry point
 * @param argc The argument counter
 * @param argv The argument vector
 * @return EXIT_SUCCESS on success, otherwise EXIT_FAILURE
 * @details Globals: g
 */
int main(int argc, char *argv[])
{
    struct options opts;
    
    parse_options(argc, argv, &opts);
    set_up_signal_handlers();

    allocate_resources();

    sem_post_or_die(g.client_request);
    sem_timedwait_or_die(g.server_available);
    if (opts.new_game)
    {
        g.shm->cmd = NEW;
        g.shm->id = GAMEID_UNDEFINED;
    }
    else
    {
        g.shm->cmd = RECONNECT;
        g.shm->id = opts.game_id;
    }

    sem_timedwait_or_die(g.server_response);
    id = g.shm->id;

    if (g.shm->state == NO_SUCH_GAME)
    {
        print_err("Game %d does not exist on server.", id);
        sem_post_or_die(g.client_read);
    }
    else
    {
        enum game_state state = g.shm->state;
        print_game(g.shm->field);
        sem_post_or_die(g.client_read);

        play(state);
    }

    free_resources();
    return EXIT_SUCCESS;
}

/**
 * @brief Scans user input and communicates with the server
 * @param state The current game state
 * @details Globals: g
 */
static void play(enum game_state state)
{
    char input;
    bool won = (state == WON);

    if ( ! won)
    {
        input = get_input();
        while (state == PLAYING)
        {
            if (is_command(input))
            {
                await_response(input);

                state = g.shm->state;
                print_game(g.shm->field);
                sem_post_or_die(g.client_read);
            
                if (input == DELETE)
                {
                    (void) printf("Delete game\n");
                    break;
                }
                else if (input == DISCONNECT)
                {
                    (void) printf("Disconnect from game\n");
                    break;
                }

                if (state == PLAYING)
                {
                    input = get_input();
                }
            }
        }
    }

    if (state == LOST)
    {
        (void) printf("Game over!\n");
    }
    else if (state == WON)
    {
        (void) printf("You won!\n");
        input = get_input_won();
        await_response(input);
        sem_post_or_die(g.client_read);
    }
}

/**
 * @brief Sends request and waits for response
 * @param command The command to send
 * @details Globals: g
 */
static void await_response(enum command cmd)
{
    sem_post_or_die(g.client_request);
    sem_timedwait_or_die(g.server_available);
    g.shm->id = id;
    g.shm->cmd = cmd;

    sem_timedwait_or_die(g.server_response);
}

/**
 * @brief Prints the controls and scans user input
 * @return The user input
 */
static char get_input(void)
{
    char input;

    (void) printf("     ^\n");
    (void) printf("    [w]\n");
    (void) printf("<[a][s][d]>   [q] disconnect from game\n");
    (void) printf("     v        [x] delete game\n");
    (void) printf("\n");
    (void) printf("Input: ");

    do
    {
        input = fgetc(stdin);
    }
    while ( ! is_command(input));
    
    return input;
}

/**
 * @brief Prints the controls available after the player won
 *        and scans user input
 * @return The user input (delete or disconnect)
 */
static char get_input_won(void)
{
    char input;

    (void) printf("[q] disconnect from game\n");
    (void) printf("[x] delete game\n");
    (void) printf("\n");
    (void) printf("Input: ");
    do
    {
        input = fgetc(stdin);
    }
    while (input != DISCONNECT && input != DELETE);

    return input;
}

/**
 * @brief checks if a char is a command
          (required to filter input-returns)
 * @param c The character to check
 * @return True if it is a command, otherwise false
 */
static bool is_command(char c)
{
    switch (c)
    {
        // intended fall through
        case UP:
        case DOWN:
        case LEFT:
        case RIGHT:
        case DELETE:
        case DISCONNECT:
        case NEW:
            return true;

        default:
            return false;
    }
}

/**
 * @brief Prints the game to STDOUT
 * @param field The game field (FIELD macro!)
 * @details Globals: g
 */
static void print_game(int FIELD)
{
    (void) printf("\n");
    (void) printf("Game #%d\n", g.shm->id);
    (void) printf("\n");

    for (int y=0; y < FIELDS_Y; y++)
    {
        for (int x=0; x < FIELDS_X; x++)
        {
            int this = field[x][y];
            if (this == FIELD_EMPTY)
            {
                (void) printf("          _");
            }
            else
            {
                int value = (int)pow(2, this);
                (void) printf(" %10d", value);
            }
        }
        (void) printf("\n");
    }

    (void) printf("\n");
}

/**
 * @brief Allocates all necessary resources
 * @details Globals: g
 */
static void allocate_resources(void)
{
    g.fd = shm_open_or_die();
    g.shm = mmap_or_die(g.fd);
    close_or_error(&g.fd);

    g.server_available = sem_open_or_die(SEM_SERVER_AVAILABLE);
    g.server_response  = sem_open_or_die(SEM_SERVER_RESPONSE);
    g.client_request   = sem_open_or_die(SEM_CLIENT_REQUEST);
    g.client_read      = sem_open_or_die(SEM_CLIENT_READ);
}

/**
 * @brief Performs a sem_open() or bail_out(), if not possible
 * @param name The semaphore name
 * @return The semaphore pointer
 */
static sem_t *sem_open_or_die(char *name)
{
    sem_t *sem = sem_open(name, 0);
    if (sem == SEM_FAILED)
    {
        print_err("Could not load resoures. Is the server running?");
        bail_out("sem_open (%s)", name);
    }
    return sem;
}

/**
 * @brief Performs a sem_timedwait() or bail_out() if not possible
 * @param sem The semaphore
 */
static void sem_timedwait_or_die(sem_t *sem)
{
    struct timespec tspec;

    if (clock_gettime(CLOCK_REALTIME, &tspec) < 0)
    {
        bail_out("clock_gettime");
    }
    tspec.tv_sec += SEM_TIMEOUT;

    if (sem_timedwait(sem, &tspec) < 0)
    {
        if (errno == ETIMEDOUT)
        {
            print_err("Semaphore timeout. Is the server still running?");
            bail_out(NULL);
        }
        else
        {
            bail_out("sem_wait");
        }
    }
}

/**
 * @brief Frees all resources (use when terminating the program)
 * @details Globals: g
 */
void free_resources(void)
{
    sigset_t blocked_signals;
    (void) sigfillset(&blocked_signals);
    (void) sigprocmask(SIG_BLOCK, &blocked_signals, NULL);

    // block signals to avoid race condition
    if (g.terminating == 1)
    {
        return;
    }
    g.terminating = 1;

    close_or_error(&g.fd);
    munmap_or_error(g.shm);

    sem_close_or_error(g.server_available);
    sem_close_or_error(g.server_response);
    sem_close_or_error(g.client_request);
    sem_close_or_error(g.client_read);
}

/**
 * @brief Parses the commandline arguments
 * @param argc The argument counter
 * @param argc The argument values
 * @param opts The options structure
 * @details Globals: g
 */
static void parse_options(int argc, char **argv, struct options *opts)
{
    int opt;
    bool new_game_init = false;
    bool game_id_init = false;

    g.progname = argv[0];

    // set defaults
    opts->new_game = true;
    opts->game_id  = GAMEID_UNDEFINED;

    while ((opt = getopt(argc, argv, "ni:")) != -1)
    {
        switch (opt)
        {
            case 'n':
                if (new_game_init)
                {
                    print_err("Argument n used more than once");
                    usage();
                }
                else
                {
                    new_game_init = true;
                    opts->new_game = true;
                }
                break;

            case 'i':
                if (game_id_init)
                {
                    print_err("Argument i used more than once");
                    usage();
                }
                else
                {
                    game_id_init = true;
                    opts->game_id = abs(str_to_int(optarg));
                    opts->new_game = false;
                }
                break;

            default:
                usage();
                break;
        }
    }

    if (new_game_init && game_id_init)
    {
        print_err("-n XOR -i");
        usage();
    }

    if (++optind < argc)
    {
        print_err("Too many arguments");
        usage();
    }
}

/**
 * @brief Prints the usage and exits the program
 * @details Globals: g
 */
static void usage(void)
{
    (void) fprintf(stderr,
        "USAGE: %s: [-n | -i <id>]\n"
        "  -n:   Start a new game\n"
        "  -i:   Connect to existing game with the given id\n", g.progname);
    exit(EXIT_FAILURE);
}
