# 2048-shell #

This is the famous game 2048 implemented in C for the shell.

### Download ###
[Built for Linux 64 bit](https://bitbucket.org/bernhard-hagmann/2048-shell/downloads/2048-linux64.tgz)

### Screenshots ###
![Client Start](doc/screenshots/client-start.png)
![Client Game Over](doc/screenshots/client-gameover.png)
![Server](doc/screenshots/server.png)
