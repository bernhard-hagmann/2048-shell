/**
 * @file   common.h
 * @author Bernhard Hagmann (1325774) <e1325774@student.tuwien.ac.at>
 * @brief  Library of common stuff for 2048
 * @date   2014-12-26
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

// ============================================================================
// Defines
// ============================================================================

#ifndef COMMON_H
#define COMMON_H

#define SHM_KEY "/2048"
#define SHM_SIZE (sizeof(struct shared))

#define SEM_SERVER_AVAILABLE "/2048-server-available"
#define SEM_SERVER_RESPONSE  "/2048-server-response"
#define SEM_CLIENT_REQUEST   "/2048-client-request"
#define SEM_CLIENT_READ      "/2048-client-read"
#define SEM_TIMEOUT 1 // sec

#define PERMISSIONS (0600)

#define GAMEID_UNDEFINED (-1)

#define FIELDS_X 4
#define FIELDS_Y 4
#define FIELD field[FIELDS_X][FIELDS_Y]

#define FIELD_EMPTY (-1)

// ============================================================================
// Structs
// ============================================================================

/** Identifies the state of a game */
enum game_state
{
    NO_SUCH_GAME,
    PLAYING,
    WON,
    LOST
};

/** Identifies user/client commands */
enum command
{
    UP    = 'w',
    LEFT  = 'a',
    DOWN  = 's',
    RIGHT = 'd',
    
    NEW        = 'n',
    RECONNECT  = 'r',
    DISCONNECT = 'q',
    DELETE     = 'x'
};

/** Shared memory structure */
struct shared
{
    // client
    enum command cmd;
    int id;

    // host
    enum game_state state;
    int field[FIELDS_X][FIELDS_Y];
};

/** Common global vars */
struct globals
{
    /** The actual progam name */
    char *progname;

    /** Used to ensure cleanup is performed only once */
    volatile sig_atomic_t terminating;

    /** Shared memory file descriptor */
    int fd;

    /** Shared memory */
    struct shared *shm;

    /** Server is available */
    sem_t *server_available;

    /** Server has written response */
    sem_t *server_response;

    /** Client has request */
    sem_t *client_request;

    /** Client has read response */
    sem_t *client_read;
} g;

// ============================================================================
// Prototypes
// ============================================================================

/**
 * @brief Frees all resources (use when terminating the program)
 */
void free_resources(void);

/**
 * @brief Prints the error message
 * @param fmt The format string
 * @param ... The format args
 * @details Globals: g.progname
 */
void print_err(const char *fmt, ...);

/**
 * @brief Prints error details and exits the program
 * @param fmt The format string
 * @param ... The format args
 * @details Globals: g.progname
 */
void bail_out(const char *fmt, ...);

/**
 * @brief Converts a string to integer or exits the programm if not possible
 * @param str The string to convert
 * @return The integer value
 */ 
int str_to_int(char *str);

/**
 * @brief Sets up the signal handlers
 */
void set_up_signal_handlers(void);

/**
 * @brief Signal handler for termination signals
 * @param sig The signal ID
 */
void signal_handler(int sig);

/**
 * @brief Performs a sem_post() or bail_out(), if not possible
 * @param sem The semaphore pointer
 */
void sem_post_or_die(sem_t *sem);

/**
 * @brief Performs a sem_close() or prints an error, if not possible
 * @param sem The semaphore pointer
 */
void sem_close_or_error(sem_t *sem);

/**
 * @brief Performs a munmap() or prints an error, if not possible
 * @param shm The shared memory struct
 */
void munmap_or_error(struct shared *shm);

/**
 * @brief Performs a close() or prints an error, if not possible
 * @param fd The file descriptor to close
 */
void close_or_error(int *fd);

/**
 * @brief Performs a shm_open() or bail_out(), if not possible
 * @return the shared memory file descriptor
 */
int shm_open_or_die(void);

/**
 * @brief Performs a mmap() or bail_out(), if not possible
 * @param fd The file descriptor
 * @return The shared memory struct
 */
struct shared *mmap_or_die(int fd);

#endif
