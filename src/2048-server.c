/**
 * @file   server.c
 * @author Bernhard Hagmann (1325774) <e1325774@student.tuwien.ac.at>
 * @brief  Server for 2048
 * @date   2014-12-24
 */

/*

SERVER:
    post server_available
    wait client_request
        ... write response ...
    post server_response
    wait client_read
    
CLIENT:
    post client_request
    wait server_available
        ... write request ...
    wait server_response
        ... read response ...
    post client_read

*/

#include <time.h>
#include "common.h"

// ============================================================================
// Defines
// ============================================================================

// ============================================================================
// Structs
// ============================================================================

/** Linked list for game data */
struct game_list
{
    /** The game field (FIELD macro!) */
    int FIELD;
    /** The game ID */
    int id;
    /** The next pointer */
    struct game_list *next;
};

// ============================================================================
// Global variables
// ============================================================================

/** The head of the game list (can be null) */
static struct game_list *head = NULL;

// ============================================================================
// Prototypes
// ============================================================================

// program management
int main(int argc, char *argv[]);
static void parse_options(int argc, char **argv, int *max_power);
static void allocate_resources(void);
static void usage(void);
static void sem_remove(sem_t *sem, char *name);
static sem_t *sem_open_or_die(char *name);
static void sem_wait_or_die(sem_t *sem);
static void memcpy_or_die(void *src, void *dest, int size);

// game management
static int add_game(void);
static void remove_game(int id);
static bool can_load_game(int id);
static struct game_list *get_game(int id);
static void load_game(int id);
static void save_game(int id);

// game logic
static void init_field(int FIELD);
static enum game_state apply_command(enum command cmd, int max_power);
static void move(int FIELD, enum command cmd);
static void move_y(int FIELD, int x, int y, int change);
static void move_x(int FIELD, int x, int y, int change);
static void spawn_number(int FIELD);
static bool is_within_bounds(int x, int y);
static enum game_state get_state(int FIELD, int max_power);
static bool has_won(int FIELD, int max_power);
static bool has_empty_fields(int FIELD);
static bool can_move(int FIELD);

// ============================================================================
// Implementations
// ============================================================================

/**
 * @brief The programs entry point
 * @param argc The argument counter
 * @param argv The argument vector
 * @return EXIT_SUCCESS on success, otherwise EXIT_FAILURE
 * @details Globals: g
 */
int main(int argc, char *argv[])
{
    int max_power;

    parse_options(argc, argv, &max_power);
    set_up_signal_handlers();

    allocate_resources();
    srand(time(NULL));

    (void) printf("Waiting for clients\n");
    while (1)
    {
        sem_post_or_die(g.server_available);
        sem_wait_or_die(g.client_request);

        g.shm->state = apply_command(g.shm->cmd, max_power);
        if (g.shm->state == WON || g.shm->state == LOST)
        {
            (void) printf("Game #%d: Over\n", g.shm->id);
        }

        sem_post_or_die(g.server_response);
        sem_wait_or_die(g.client_read);
    }

    free_resources();
    return EXIT_SUCCESS;
}

static void sem_wait_or_die(sem_t *sem)
{
    if (sem_wait(sem) < 0)
    {
        bail_out("sem_wait");
    }
}

/**
 * @brief Creates a new game and adds it to the back of the list
 * @return The ID of the new game
 * @details Globals: head
 */
static int add_game()
{
    struct game_list *new = (struct game_list *) malloc(sizeof(struct game_list));
    if (new == NULL)
    {
        bail_out("malloc");
    }

    init_field(new->field);
    new->next = NULL;

    if (head == NULL)
    {
        new->id = 0;
        head = new;
    }
    else
    {
        struct game_list *curr = head;
        while (curr != NULL && curr->next != NULL)
        {
            curr = curr->next;
        }
        // got here -> curr = last game in list
        new->id = curr->id + 1;
        curr->next = new;
    }

    return new->id;
}

/**
 * @brief Deletes the game with the given id
 * @param id The game id
 * @details Globals: head
 */
static void remove_game(int id)
{
    struct game_list *curr = head;
    struct game_list *prev = NULL;

    if (curr == NULL)
    {
        print_err("Game %d: Delete -- ERROR: Game does not exist", id);
    }

    while (curr->id != id && curr->next != NULL)
    {
        prev = curr;
        curr = curr->next;
    }
    
    if (curr->id == id)
    {
        if (curr->next != NULL) // has following nodes
        {
            if (prev == NULL)
            {
                head = curr->next;
            }
            else
            {
                prev->next = curr->next;
            }
        }
        else if (prev == NULL) // is head
        {
            head = NULL;
        }
        else // not head, no following nodes
        {
            prev->next = NULL;
        }

        free(curr);
    }
    else
    {
        print_err("Game %d: Delete -- ERROR: Game does not exist", id);
    }
}

/**
 * @brief Checks if a game with specific ID can be loaded
 * @return True if it can be loaded, otherwise false
 * @details Globals: head
 */
static bool can_load_game(int id)
{
    struct game_list *curr = head;
    while (curr != NULL)
    {
        if (curr->id == id)
        {
            return true;
        }
        curr = curr->next;
    }

    return false;
}

/**
 * @brief Loads a game into the shared memory
 * @param id The game ID
 * @details Globals: g
 */
static void load_game(int id)
{
    struct game_list *game = get_game(id);
    memcpy_or_die(game->field, g.shm->field, sizeof(g.shm->field));
}

/**
 * @brief Saves a game from shared memory into the local list
 * @param id The game ID
 * @details Globals: g
 */
static void save_game(int id)
{
    struct game_list *game = get_game(id);
    memcpy_or_die(g.shm->field, game->field, sizeof(g.shm->field));
}

/**
 * @brief Gets a game from the local list
 * @param id The game ID
 * @details Globals: head
 */
static struct game_list *get_game(int id)
{
    struct game_list *curr = head;

    if ( ! can_load_game(id))
    {
        print_err("Game %d: Load -- ERROR: Game does not exist", id);
        bail_out(NULL);
    }

    while (curr->id != id && curr->next != NULL)
    {
        curr = curr->next;
    }
    assert(curr->id == id);

    return curr;
}

/**
 * @brief Performs a memcpy() or bail_out(), if not possible
 */
static void memcpy_or_die(void *src, void *dest, int size)
{
    if (memcpy(dest, src, size) == NULL)
    {
        bail_out("memcpy");
    }
}

/**
 * @brief Initializes a game field
 * @param field The game field (FIELD macro!)
 */
static void init_field(int FIELD)
{
    for (int y=0; y < FIELDS_Y; y++)
    {
        for (int x=0; x < FIELDS_X; x++)
        {
            field[x][y] = FIELD_EMPTY;
        }
    }
}

/**
 * @brief Applies a user/client command to a game
 * @param cmd The command
 * @param max_power The winning condition (2^power)
 * @return The new game state
 * @details Globals: g
 */
static enum game_state apply_command(enum command cmd, int max_power)
{
    enum game_state state = NO_SUCH_GAME;
    int id = g.shm->id;

    switch (cmd)
    {
        // intended fall-through for moves
        case UP:
        case LEFT:
        case DOWN:
        case RIGHT:
            load_game(id);
            move(g.shm->field, cmd);
            spawn_number(g.shm->field);
            save_game(id);
            state = get_state(g.shm->field, max_power);
            break;

        case NEW:
            id = add_game();
            
            load_game(id);
            spawn_number(g.shm->field);
            spawn_number(g.shm->field);
            save_game(id);
            
            g.shm->id = id;
            state = PLAYING;
            (void) printf("Game #%d: New\n", id);
            break;

        case RECONNECT:
            if (can_load_game(id))
            {
                load_game(id);
                state = get_state(g.shm->field, max_power);
                (void) printf("Game #%d: Client reconnected\n", id);
            }
            else
            {
                state = NO_SUCH_GAME;
                print_err("Game %d: Reconnect -- ERROR: Game does not exist", id);
            }
            break;

        case DISCONNECT:
            // nothing to do
            break;
    
        case DELETE:
            remove_game(id);
            (void) printf("Game #%d: Deleted\n", id);
            break;

        default:
            // should never get here
            assert(0);
            break;
    }

    return state;
}

/**
 * @brief Performs a move command
 * @param field The game field (FIELD macro!)
 * @param cmd The user/client command
 */
static void move(int FIELD, enum command cmd)
{
    switch (cmd)
    {
        case UP:
            // process top to bottom
            for (int y=0; y < FIELDS_Y; y++)
            {
                for (int x=0; x < FIELDS_X; x++)
                {
                    move_y(field, x, y, -1);
                }
            }
            break;

        case LEFT:
            // process left to right
            for (int y=0; y < FIELDS_Y; y++)
            {
                for (int x=0; x < FIELDS_X; x++)
                {
                    move_x(field, x, y, -1);
                }
            }
            break;

        case DOWN:
            // process bottom to top 
            for (int y=FIELDS_Y-1; y >= 0; y--)
            {
                for (int x=0; x < FIELDS_X; x++)
                {
                    move_y(field, x, y, +1);
                }
            }
            break;

        case RIGHT:
            // process right to left
            for (int y=0; y < FIELDS_Y; y++)
            {
                for (int x=FIELDS_X-1; x >= 0; x--)
                {
                    move_x(field, x, y, +1);
                }
            }
            break;

        default:
            // should never get here
            assert(0);
            break;
    }
}

/**
 * @brief Moves a game field vertically
 * @param field  The game field (FIELD macro!)
 * @param x      x-Coordinate of the cell to move
 * @param y      y-Coordinate of the cell to move
 * @param change The direction (-1 = up, +1 = down)
 */
static void move_y(int FIELD, int x, int y, int change)
{
    // move over empty fields
    int nextY = y + change;
    while (is_within_bounds(x, nextY) && field[x][nextY] == FIELD_EMPTY)
    {
        field[x][nextY] = field[x][y];
        field[x][y] = FIELD_EMPTY;
        y = nextY;
        nextY += change;
    }

    // possibly merge with remaining field
    if (is_within_bounds(x, nextY) && field[x][nextY] == field[x][y])
    {
        ++field[x][nextY];
        field[x][y] = FIELD_EMPTY;
    }
}

/**
 * @brief Moves a game field horizontally
 * @param field  The game field (FIELD macro!)
 * @param x      x-Coordinate of the cell to move
 * @param y      y-Coordinate of the cell to move
 * @param change The direction (-1 = left, +1 = right)
 */
static void move_x(int FIELD, int x, int y, int change)
{
    // move over empty fields
    int nextX = x + change;
    while (is_within_bounds(nextX, y) && field[nextX][y] == FIELD_EMPTY)
    {
        field[nextX][y] = field[x][y];
        field[x][y] = FIELD_EMPTY;
        x = nextX;
        nextX += change;
    }

    // possibly merge with remaining field
    if (is_within_bounds(nextX, y) && field[nextX][y] == field[x][y])
    {
        ++field[nextX][y];
        field[x][y] = FIELD_EMPTY;
    }
}

/**
 * @brief Checks if a point is within the game field
 * @param x The x-coordinate
 * @param y The y-cooridnate
 * @return True if it is within the field, otherwise false
 */
static bool is_within_bounds(int x, int y)
{
    return (x >= 0)
        && (x < FIELDS_X)
        && (y >= 0)
        && (y < FIELDS_Y);
}

/**
 * @brief Spawns a number in an empty field
 *        (if there are any empty fields)
 *        75 % chance: 2
 *        25 % chance: 4
 * @param field The game field (FIELD macro!)
 */
static void spawn_number(int FIELD)
{
    if (has_empty_fields(field))
    {
        int x;
        int y;
        int power;

        do
        {
            x = rand() % FIELDS_X;
            y = rand() % FIELDS_Y;
        }
        while (field[x][y] != FIELD_EMPTY);

        power = (rand() % 99 < 75) ? 1 : 2;
        field[x][y] = power;
    }
}

/**
 * @brief Analyses the state of a game
 * @param field The game field (FIELD macro!)
 * @param max_power The winning condition (2^power)
 * @return The game state
 */
static enum game_state get_state(int FIELD, int max_power)
{
    if (has_won(field, max_power))
    {
        return WON;
    }
    else if (has_empty_fields(field) || can_move(field))
    {
        return PLAYING;
    }
    else
    {
        return LOST;
    }
}

/**
 * @brief Checks if the user has won
 * @param field The game field (FIELD macro!)
 * @param max_power The winning condition (2^power)
 * @return True if the user has won, otherwise false
 */
static bool has_won(int FIELD, int max_power)
{
    for (int y=0; y < FIELDS_Y; y++)
    {
        for (int x=0; x < FIELDS_X; x++)
        {
            int this = field[x][y];
            if (this >= max_power)
            {
                return true;
            }
        }
    }

    return false;
}

/**
 * @brief Checks if any move is possible
 * @param field The game field (FIELD macro!)
 * @return True if the a move is possible, otherwise false
 */
static bool can_move(int FIELD)
{
    for (int y=0; y < FIELDS_Y; y++)
    {
        for (int x=0; x < FIELDS_X; x++)
        {
            int this = field[x][y];
            if ( (x < FIELDS_X-1 && field[x+1][y] == this)
              || (y < FIELDS_Y-1 && field[x][y+1] == this) )
            {
                return true;
            }
        }
    }

    return false;
}

/**
 * @brief Checks if there are empty fields
 * @param field The game field (FIELD macro!)
 * @return True if there are empty fields, otherwise false
 */
static bool has_empty_fields(int FIELD)
{
    for (int y=0; y < FIELDS_Y; y++)
    {
        for (int x=0; x < FIELDS_X; x++)
        {
            if (field[x][y] == FIELD_EMPTY)
            {
                return true;
            }
        }
    }

    return false;
}

/**
 * @brief Allocates all necessary resources
 * @details Globals: g
 */
static void allocate_resources(void)
{
    g.fd = shm_open_or_die();

    if (ftruncate(g.fd, SHM_SIZE) < 0)
    {
        bail_out("ftruncate");
    }

    g.shm = mmap_or_die(g.fd);
    close_or_error(&g.fd);

    g.server_available = sem_open_or_die(SEM_SERVER_AVAILABLE);
    g.server_response  = sem_open_or_die(SEM_SERVER_RESPONSE);
    g.client_request   = sem_open_or_die(SEM_CLIENT_REQUEST);
    g.client_read      = sem_open_or_die(SEM_CLIENT_READ);
}

/**
 * @brief Performs a sem_open() or bail_out(), if not possible
 * @param name The semaphore name
 * @return The semaphore pointer
 */
static sem_t *sem_open_or_die(char *name)
{
    sem_t *sem = sem_open(name, O_CREAT | O_EXCL, PERMISSIONS, 0);
    if (sem == SEM_FAILED)
    {
        bail_out("sem_open (%s)", name);
    }
    return sem;
}

/**
 * @brief Frees all resources (use when terminating the program)
 * @details Globals: g, head
 */
void free_resources(void)
{
    sigset_t blocked_signals;
    (void) sigfillset(&blocked_signals);
    (void) sigprocmask(SIG_BLOCK, &blocked_signals, NULL);

    // block signals to avoid race condition
    if (g.terminating == 1)
    {
        return;
    }
    g.terminating = 1;

    // free memory
    {
        struct game_list *curr = head;
        struct game_list *next = NULL;
        while (curr != NULL)
        {
            next = curr->next;
            free(curr);
            curr = next;
        }
    }

    close_or_error(&g.fd);
    munmap_or_error(g.shm);
    sem_remove(g.server_available, SEM_SERVER_AVAILABLE);
    sem_remove(g.server_response,  SEM_SERVER_RESPONSE);
    sem_remove(g.client_request,   SEM_CLIENT_REQUEST);
    sem_remove(g.client_read,      SEM_CLIENT_READ);

    if (shm_unlink(SHM_KEY) < 0)
    {
        print_err("shm_unlink failed");
    }
}

/**
 * @brief Removes a semaphore or prints an error
 * @param sem The semaphore pointer
 * @param name The semaphore name
 */
static void sem_remove(sem_t *sem, char *name)
{
    sem_close_or_error(sem);

    if (sem_unlink(name) < 0)
    {
        print_err("sem_unlink (%s) failed", name);
    }
}

/**
 * @brief Parses the commandline arguments
 * @param argc The argument counter
 * @param argc The argument values
 * @param max_power The maximum power (will be set)
 * @details Globals: g
 */
static void parse_options(int argc, char **argv, int *max_power)
{
    int opt;
    bool max_power_init = false;

    // set default value
    *max_power = 11;

    g.progname = argv[0];

    while ((opt = getopt(argc, argv, "p:")) != -1)
    {
        switch (opt)
        {
            case 'p':
                if (max_power_init)
                {
                    print_err("Argument p used more than once");
                    usage();
                }
                else
                {
                    max_power_init = true;
                    *max_power = abs(str_to_int(optarg));
                }
                break;

            default:
                usage();
                break;
        }
    }

    if (++optind < argc)
    {
        print_err("Too many arguments");
        usage();
    }
}

/**
 * @brief Prints the usage and exits the program
 * @details Globals: g
 */
static void usage(void)
{
    (void) fprintf(stderr,
        "USAGE: %s: [-p power_of_two]\n"
        "  -p:   Play until 2^power_of_two is reached (default: 11)\n", g.progname);
    exit(EXIT_FAILURE);
}
