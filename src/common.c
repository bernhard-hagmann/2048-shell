/**
 * @file   common.c
 * @author Bernhard Hagmann (1325774) <e1325774@student.tuwien.ac.at>
 * @brief  Common functions for 2048
 * @date   2014-12-29
 */

#include "common.h"

// ============================================================================
// Implementations
// ============================================================================

void print_err(const char *fmt, ...)
{
    va_list ap;

    (void) fprintf(stderr, "%s: ", g.progname);

    va_start(ap, fmt);
    (void) vfprintf(stderr, fmt, ap);
    va_end(ap);

    (void) fprintf(stderr, "\n");
}

void bail_out(const char *fmt, ...)
{
    if (fmt != NULL)
    {
        va_list ap;

        (void) fprintf(stderr, "%s: ", g.progname);
        va_start(ap, fmt);
        (void) vfprintf(stderr, fmt, ap);
        va_end(ap);

        if (errno != 0)
        {
            (void) fprintf(stderr, ": %s", strerror(errno));
        }
        (void) fprintf(stderr, "\n");
    }

    free_resources();
    exit(EXIT_FAILURE);
}

int str_to_int(char *str)
{
    char *end;
    int val;

    errno = 0;
    val = (int) strtol(optarg, &end, 10);
    if ( (errno == ERANGE)
         || (errno != 0 && val == 0) )
    {
        bail_out("str_to_int - strtol");
    }

    if (end == str)
    {
        bail_out("str_to_int - no digits found");
    }

    // got here -> successfully parsed a number

    if (*end != '\0')
    {
        bail_out("str_to_int - further characters: %s", end);
    }

    return val;
}

void signal_handler(int sig)
{
    (void) printf("Received quit signal\n");
    free_resources();
    exit(EXIT_SUCCESS);
}

void set_up_signal_handlers()
{
    sigset_t blocked_signals;

    if (sigfillset(&blocked_signals) < 0)
    {
        bail_out("sigfillset");
    }
    else
    {
        const int signals[] = { SIGINT, SIGTERM };
        struct sigaction sa;
        sa.sa_handler = signal_handler;
        (void) memcpy(&sa.sa_mask, &blocked_signals, sizeof(sa.sa_mask));
        sa.sa_flags = SA_RESTART;
        for (int i=0; i < sizeof(signals)/sizeof(signals[0]); i++)
        {
            if (sigaction(signals[i], &sa, NULL) < 0)
            {
                bail_out("sigaction");
            }
        }
    }
}

void sem_post_or_die(sem_t *sem)
{
    if (sem_post(sem) < 0)
    {
        bail_out("sem_post");
    }
}


void sem_close_or_error(sem_t *sem)
{
    if (sem != NULL)
    {
        if (sem_close(sem) < 0)
        {
            print_err("sem_close failed");
        }
    }
}

void munmap_or_error(struct shared *shm)
{
    if (munmap(shm, SHM_SIZE) < 0)
    {
        print_err("munmap failed");
    }
}

void close_or_error(int *fd)
{
    if (*fd != -1)
    {
        if (close(*fd) < 0)
        {
            print_err("close failed");
        }
        else
        {
            *fd = -1;
        }
    }
}

int shm_open_or_die(void)
{
    int fd = shm_open(SHM_KEY, O_RDWR | O_CREAT, PERMISSIONS);
    if (fd < 0)
    {
        bail_out("shm_open");
    }
    return fd;
}

struct shared *mmap_or_die(int fd)
{
    struct shared *shm = mmap(NULL, SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shm == MAP_FAILED)
    {
        bail_out("mmap");
    }
    return shm;
}
